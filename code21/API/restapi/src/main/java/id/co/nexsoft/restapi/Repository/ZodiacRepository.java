package id.co.nexsoft.restapi.Repository;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.restapi.Model.Zodiac;

public interface ZodiacRepository extends CrudRepository<Zodiac, Integer> {
    Zodiac save(Zodiac zodiac);
}
