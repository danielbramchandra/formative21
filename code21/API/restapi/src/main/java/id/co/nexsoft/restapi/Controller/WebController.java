package id.co.nexsoft.restapi.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import id.co.nexsoft.restapi.Model.Zodiac;
import id.co.nexsoft.restapi.Repository.ZodiacRepository;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import java.sql.*;


@Controller
public class WebController {
    @Autowired
    ZodiacRepository repo;
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping(value="/add",consumes = "application/json")
    public String add(@RequestBody Zodiac zodiac) {     
        
    repo.save(zodiac);
        return "/";
    }
   
    
    
}
