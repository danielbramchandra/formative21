import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>INPUT NAMA</h2>
        <input id="name" type="text"></input>
        <h2>INPUT TANGGAL LAHIR</h2>
        <input type="date" id="birthday" name="birthday"></input>
        <br></br>
        <button className="button button2" id="submitDate" onClick={findZodiac}>
          Find Zodiac
        </button>
        <h3 id="result"></h3>
      </header>
    </div>
  );
}
function findZodiac() {
  let birthdate = document.getElementById("birthday").value;
  let birthday = parseInt(birthdate.substring(8, 10));
  let birthmonth = parseInt(birthdate.substring(5, 7));
  let result;
  if ((birthmonth === 1 && birthday >= 20) || (birthmonth === 2 && birthday <= 18)) {
    result = "Aquarius";
  }

  if ((birthmonth === 2 && birthday >= 19) || (birthmonth === 3 && birthday <= 20)) {
    result = "Pisces";
  }
  if ((birthmonth === 3 && birthday >= 21) || (birthmonth === 4 && birthday <= 19)) {
    result = "Aries";
  }

  if ((birthmonth === 4 && birthday >= 20) || (birthmonth === 5 && birthday <= 20)) {
    result = "Taurus";
  }

  if ((birthmonth === 5 && birthday >= 21) || (birthmonth === 6 && birthday <= 20)) {
    result = "Gemini";
  }
  if ((birthmonth === 6 && birthday >= 21) || (birthmonth === 7 && birthday <= 22)) {
    result = "Cancer";
  }
  if ((birthmonth === 7 && birthday >= 23) || (birthmonth === 8 && birthday <= 22)) {
    result = "Leo";
  }
  if ((birthmonth === 8 && birthday >= 23) || (birthmonth === 9 && birthday <= 22)) {
    result = "Virgo";
  }
  if ((birthmonth === 9 && birthday >= 23) || (birthmonth === 10 && birthday <= 22)) {
    result = "Libra";
  }
  if ((birthmonth === 10 && birthday >= 23) || (birthmonth === 11 && birthday <= 21)) {
    result = "Scorpio";
  }
  if ((birthmonth === 11 && birthday >= 22) || (birthmonth === 12 && birthday <= 21)) {
    result = "Sagittarius";
  }
  if ((birthmonth === 12 && birthday >= 22) || (birthmonth === 1 && birthday <= 19)) {
    result = "Capricorn";
  }
  document.getElementById("result").innerHTML = result;
  postData(birthdate, result);
}
function postData(birthdate, result) {
  let data = {
    name: document.getElementById("name").value,
    date: birthdate,
    zodiac: result,
  };
  fetch("http://localhost:8080/add", {
    // Adding method type
    method: "POST",

    // Adding body or contents to send
    body: JSON.stringify(data),

    // Adding headers to the request
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  });
}
export default App;
