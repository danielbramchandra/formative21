import logo from "./logo.svg";
import "./App.css";

let date = new Date().toDateString();
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h2>Hello World</h2>
        <h4>{date}</h4>
      </header>
    </div>
  );
}

export default App;
